# UniversityStudentManagementSystem
The university student management system is a Java command-line application that provides a comprehensive solution for managing student data.

## Description & Usage
The system supports three types of users: admin, faculty, and student. 

The admin is responsible for creating and removing student and faculty accounts, while students and faculty members can log in to perform various tasks related to student data.

Students can log in to view their biodata, attendance, fees, grades and library details. On the other hand, faculty members can make updates on the student’s data and generate reports on student data, making it easy for faculty members to analyse and track student progress. 

The system stores all student data in a secure database and provides quick and easy access to the required data. 


## Software Req
Ecllipse (Java)\
MySql-Workbench

## Installation steps and view
Download the project\
Configure Buildpath for external jar\
load the script in mysql having sample testing database and tables with some values\
run the main file in com_ui package

## Authors and Contributors - by Cohort 6 ( Group-9 )
1.Gowdaperu Vineesha\
2.Karimolla Pranusha\
3.Sriya korupoju\
4.Kudumula Ravali\
5.Immadisetty Anvith\
6.Nikhil Chandramauli\
7.Prem sagar Aremanda 


## Project Concepts
This system includes and covers most of the concepts in java like oops, interfaces, collections, exception handling, file handling, JDBC, layers application, etc…